var winWidth= 750
var winHeigth=500

let video;
let poseNet;
let pose;
let skeleton;

var frameCount=0

let env, wave;

function setup() {
  const canvas= createCanvas(winWidth, winHeigth);
  canvas.parent('webcam-div')

  env = new p5.Envelope();
  env.setADSR(0.05, 0.1, 0.5, 1);
  env.setRange(1.2, 0);

  wave = new p5.Oscillator();
  wave.setType('sine');
  wave.start();
  wave.freq(440);
  wave.amp(env);


  video = createCapture(VIDEO);
  video.hide();
  
  poseNet = ml5.poseNet(video, modelLoaded);
  poseNet.on('pose', gotPoses);

}

function gotPoses(poses) {
  if (poses.length > 0) {
    pose = poses[0].pose;
    skeleton = poses[0].skeleton;
  }
}

function modelLoaded() {
  console.log('poseNet ready');
}



function draw(){
 /*
  background(2,2,2)
  var value=mapWindowFrequency(mouseX, mouseY)
  wave.freq(value);
  env.play();
*/

  push();
  translate(winWidth, 0);
  scale(-1, 1);
  image(video, 50, 10);


  if (pose) {
    
    var value=mapWindowFrequency(pose.keypoints[0].position.x, pose.keypoints[0].position.y)
    wave.freq(value);
    env.play();

    fill(random(250),random(250),random(250))
    ellipse(pose.keypoints[0].position.x, pose.keypoints[0].position.y,15)

    background(200+rgbMap(value),random(rgbMap(value)),200+rgbMap(value),1)

  }

  pop();

  frameCount++
  if(frameCount>300){
    frameCount=0
  }
  if(frameCount%100==0)divChange()
  if(frameCount%20==0)generateRandomText()

}


function mapWindowFrequency(x,y){

  var value=x-y

  return map(value,0,winHeigth,260.6256,500.8833)
}

function inverseMap(freq){
  return map(freq,260.6256,500.88330,winHeigth/2,winHeigth)
}

function rgbMap(freq){
  return map(freq,260.6256,500.88330,0,255)
}

var colorsArray=['rgb(109, 270, 209)','rgb(243, 179, 229)','rgb(122, 255, 211)','rgb(248, 155, 233)']
function divChange(){

  var posArray=[0,1,2,3]
  posArray = posArray.sort(() => Math.random() - 0.5) //shuffle positions


  var div1=document.getElementById("div1").style.backgroundColor =colorsArray[posArray[0]].toLowerCase()   //Math.floor(Math.random() * colorsArray.length) 
  var div2=document.getElementById("div2").style.backgroundColor =colorsArray[posArray[1]].toLowerCase()
  var div3=document.getElementById("div3").style.backgroundColor =colorsArray[posArray[2]].toLowerCase()
  var div4=document.getElementById("webcam-div").style.backgroundColor =colorsArray[posArray[3]].toLowerCase()

  
}


function generateRandomText() {
  const characters = 'ر ز ط ظ ك ل م ن ص ض';
  let result = ' ';
  const charactersLength = characters.length;
  for(let i = 0; i < 200; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  var doc=document.getElementsByClassName("randomText")

  for(let i=0;i<doc.length;i++){
    doc[i].textContent = result

  }

}

