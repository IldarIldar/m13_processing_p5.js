function setup() {
  // put setup code here
  createCanvas(windowWidth,windowHeight)//width, height
  background(200) //color o luz
  frameRate(20)
}

var r=0 //red and blue
var b=255

function draw() {

//background
r=map(mouseX,0,windowWidth,0,255)//funcion map transforma un set a otro, en este caso el tamaño de la pantalla al color
b=map(mouseX,0,windowWidth,255,0)
background(r,0,b)

//ellipse
rFill=map(mouseX,0,windowWidth,255,0)
bFill=map(mouseX,0,windowWidth,0,255)
fill(rFill,0,bFill)
ellipse(mouseX,200,65,65)

}

//canvia el tamaño de canvas cuando cambiamos de tamaño la ventana
function windowResized(){   
  resizeCanvas(windowWidth,windowHeight)
  background(200) //color o luz
}

