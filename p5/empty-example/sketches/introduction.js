let t = 0; // time variable
var frameCount=0;
let env
let wave;

var soundOn=false

let t1 = 0.1; // attack time in seconds
let l1 = 0.7; // attack level 0.0 to 1.0
let t2 = 0.1; // decay time in seconds
let l2 = 0; // decay level  0.0 to 1.0

var plato, lata,chimes;

function preload(){
  getAudioContext().suspend();


  plato=loadSound("./src/sounds/platodurisimo.wav")
  lata=loadSound("./src/sounds/lata6.mp3")
  chimes=loadSound("./src/sounds/chimes.wav")
  hiHat=loadSound("./src/sounds/hiHat.wav")
  
}

function setup() {
  getAudioContext().suspend();

  const canvas= createCanvas(600, 600);
  canvas.parent('script-container')
}

var freq
function draw() {
  background(10, 10); // translucent background (creates trails)
  if(frameCount%5==0){
  noStroke();
  //fill(random(40), random(170), random(170),220);
  fill(random(240), random(220), random(240),150+mouseX/3);
  }

  if(soundOn){
    if(frameCount%5==0){
    
      //print("entra")
      freq=setFrequency(mouseY)+(mouseX/20)
      wave.freq(freq-random(5));
      env.play();
      }
      rythms()
  }
  // make a x and y grid of ellipses
  for (let x = 0; x <= width; x = x + 35) {
    for (let y = 0; y <= height; y = y + 35) {
      // starting point of each circle depends on mouse position
      const xAngle = map(57, 0, width, -4 * PI, 4 * PI, true)/4;
      const yAngle = map(191, 0, height, -4 * PI, 4 * PI, true)*20;
      // and also varies based on the particle's location
      const angle = xAngle * (x / width) + yAngle * (y / height);

      // each particle moves in a circle
      const myX = x + 20 * cos(2 * PI * t + angle)*mouseX/2000

      const myY = y + 20 * sin(2 * PI * t + angle)*mouseY/200

      ellipse(myX, myY, 10); // draw particle
    }
  }

  t = t + 0.01; // update time
  if(frameCount>400){
      frameCount=0
  }
  frameCount++;

}


//Frequency with mouse
function setFrequency(mousePos){
 let freq

 var mouseToInt=map(mousePos, 0, windowHeight,1,8); //we transform the mouse pos to a position in  Major scale
 mouseToInt=parseInt(mouseToInt) 
  switch (mouseToInt) {
    case 1:  freq=261
      break;
    case 2: freq= 329
      break;

    case 3: freq= 293
      break;

    case 4:freq= 391
      break;
    
    case 5: freq=  349
      break;
    case 6: freq= 493
      break;  
 
      case 7: freq= 523
      break;  
      
      case 8: freq= 440
      break;
    default: freq= 391
      break;   
    }

   // print(freq)
  return freq  
}




//Sampling
var platoT=400
var hiHatT=25
var lataT=100

function rythms(){

  //var platoT= document.getElementById("hat").value

  
  hiHatT=map(document.getElementById("hihat").value,
          10,30,
          30,10)
  //var lataT=document. getElementById("hihat").value


  if(frameCount%platoT==0){
    plato.play()
  }
  if(frameCount%330==0){
    chimes.play()
  }
  if(frameCount%hiHatT==0){
    hiHat.play()
  }
  if(frameCount%lataT==0){
    lata.play()
  }

}


//Sound on and off
var control=0
function soundOffOn(){


  env = new p5.Envelope(t1, l1, t2, l2);
  //  env.setADSR(0.05, 0.1, 0.5, 1);
  //  env.setRange(1.2, 0);
  
  wave = new p5.Oscillator();
  wave.setType('triangle');
  wave.start();
  wave.amp(env);
  

  control+=1
  print(control)
  if(control%2==0){ 
  soundOn=false
  }else{
    soundOn=true
  }
}

function mousePressed(){
  userStartAudio();

}