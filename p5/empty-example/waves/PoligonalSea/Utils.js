function randomRange(min, max){
    return  Math.random() * (max - min) + min;
  }

  function modulus(v){
    return Math.sqrt( v.x * v.x + v.y * v.y + v.z * v.z) //modulo= raiz cuadrada(x^2+y^2) **es la longitud del vector
  }

  function distance(a, b){
      var ab = {
        x:b.x-a.x,
        y:b.y-a.y,
        z:b.z-a.z
      }

      return modulus(ab)  
  }

  function LaGrangeWave(x,d,offset){

    return ((x-d-offset) * (x-d+offset )) /(-offset*offset)
  }