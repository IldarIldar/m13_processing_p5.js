var waveType="no wave"
var waveType2D="sine realistic"

var lastFrameFocused = false
var mode3D=false
var changeCamera3d=false

var updateF=false //updates frequency
var updateA=false //updates amplitude
var updateM=false //updates movement

var wave
var sliderA,sliderF,sliderM

//modificadores
var fMod=1/1500
var aMod=1/2
var mMod=1/100

//ranges html docs
var rangeF=document.getElementById("rangeF")
var rangeA=document.getElementById("rangeA")
var rangeM=document.getElementById("rangeM")

function randomRange(min, max){
  return  Math.random() * (max - min) + min;
}

function changeUpdate(){
  updateF=document.getElementById("freqAut").checked
  updateA=document.getElementById("ampAut").checked
  updateM=document.getElementById("movmAut").checked


}

function changeMode(){
  mode3D=!mode3D
  print(mode3D)

}

function preload(){
  font =loadFont('src/blackHelvetica.otf');
}

function setup() {

    const canvas= createCanvas(winWidth,winHeig,WEBGL)//width, height
    canvas.parent('script-container')

    background(240)
    seaSetup()   

    sea.noOnda=true


    wave=new Wave(0.07,Math.PI*2,16,2.5,0.5,1,1,1,1,1,1)

  changeType()
}


function draw() {

  if(mode3D){
    if(changeCamera3d){
      Camera.y=0
      Camera.x=402
      changeCamera3d=false
    }
      clear()
      background(255)

      fill(20)
      textFont(font);
      textSize(15)
      text(waveType, 100, 0)

      
      var dT=deltaTime/1000.0 //delta is in miliseconds /1000=seconds

    
      for (let i = 0; i < sea.waves.length; i++) {
        sea.waves[i].update(dT);
      }
      
      translate(-Camera.x, -Camera.y, -Camera.z)
      //translate(circularWaveCenter.x, circularWaveCenter.y, circularWaveCenter.z)
      //noStroke()
      //fill(255,230,0)
      //sphere(30)
      //translate(-circularWaveCenter.x, -circularWaveCenter.y, -circularWaveCenter.z)
      
      
      
      translate(Camera.x, Camera.y, Camera.z)   
      
      //specularMaterial(250)
      

      //ambientLight(100, 60, 60);
      //pointLight(200, 100, 10, Camera.x, Camera.y, Camera.z-600);
      fill(100,100,255)
      stroke(0, 0,255, 150)

      Camera.update(dT);
      drawDrawableObjects()
      
      
        
  
  
  }else{
      if(!changeCamera3d){
        Camera.y=-400
        Camera.x=-402
        changeCamera3d=true
      }
    translate(Camera.x, Camera.y, Camera.z)   

    background(240)

    fill(20)
    textFont(font);
    textSize(15)
    text(waveType2D, 100, 82)


    if(waveType2D=="sine wave with params"){
      if(updateF||updateA||updateM){
        updateFunction()
       
      }
      wave.l=document.getElementById("rangeF").value*fMod
      wave.a=document.getElementById("rangeA").value*aMod    
      wave.t=document.getElementById("rangeM").value*mMod

    }

    var dT=deltaTime/1000.0 //delta is in miliseconds /1000=seconds


    let nextXNode=0

    if(waveType2D=="sine wave realistic" || mode3D){
      wave.update(dT)
    }




    for(let i=0; i<edgesNum;i++){


        let p1 = wave.getParticlePos(nextXNode)//x1  
                              
        let p2 = wave.getParticlePos(nextXNode+edgesLength) //x2

        line(
          p1.x,
          p1.y+halfY,
          p2.x,
          p2.y+halfY
          )

        nextXNode=nextXNode+edgesLength //next x pos + edgeLength
      
      }

      translate(-Camera.x, -Camera.y, -Camera.z)

  }
}



var maxSlideA=true
var maxSlideM=true
var maxSlideF=true

function updateFunction(){
  

 var f= document.getElementById("rangeF").value
 var a= document.getElementById("rangeA").value
 var m=document. getElementById("rangeM").value

  if(f>=100){
    maxSlideF=true
  }else if(f<=1){
    maxSlideF=false
  }

  if(a>=100 ){
    maxSlideA=true
  }else if(a<1 ){
    maxSlideA=false
  }

  if(m>=628){
    maxSlideM=true
  }else if(m<=1){
    maxSlideM=false
  }
  


  if(maxSlideF && updateF){
    f--
  }else if(!maxSlideF && updateF){
    f++
  }

  if(maxSlideA && updateA){
    a--
  }else if(!maxSlideA && updateA){
    a++
  }

  if(maxSlideM && updateM){
    m=0
  }else if(!maxSlideM && updateM){
    m++
  }
 
 if(updateA) document.getElementById("rangeA").value=a

 if(updateF) document.getElementById("rangeF").value=f

 if(updateM) document. getElementById("rangeM").value=m

  
}



function changeType(){
 
 if(mode3D){
      if(waveType=="no wave"){
        
        waveType="basic wave"
        sea.ondaBase=true
        sea.ondaCircular=false
        sea.ondaLaGrange=false
        sea.noOnda=false

      }else if(waveType=="basic wave"){
        waveType="circular wave"
        sea.ondaBase=false
        sea.ondaCircular=true
        sea.ondaLaGrange=false
        sea.noOnda=false

      }else if(waveType=="circular wave"){
        waveType="realistic wave"
        sea.ondaBase=true
        sea.ondaCircular=true
        sea.ondaLaGrange=false
        sea.noOnda=false

      }else if(waveType=="realistic wave"){
        waveType="la grange curves"
        sea.ondaBase=false
        sea.ondaCircular=true
        sea.ondaLaGrange=true
        sea.noOnda=false

      }else{ //la grange
        waveType="no wave"
        sea.ondaBase=false
        sea.ondaCircular=false
        sea.ondaLaGrange=false
        sea.noOnda=true

      }

 }else{
      var freqText=document.getElementById("freq")
      var ampText=document.getElementById("amp")
      var movText=document.getElementById("movm")

      if(waveType2D=="sine wave"){
        waveType2D="sine wave with params"

        freqText.style.color='black'
        ampText.style.color='black'
        movText.style.color='black'

      }else if(waveType2D=="sine wave with params"){
        wave = new Wave(
          0.00577,      //l 
          Math.PI*2,   //t 
          16.33,      //a 
          0.38,      //k
          0.06,     //oscLmax, 
          0.04,    //oscLmin 
          0.0012, //oscLspeed,
          16,    //oscAmax, 
          -3,   //oscAmin
          16,  //oscAspeed
          0.4 //speedTimeIncrem
          )
          waveType2D="sine wave realistic"

          freqText.style.color='red'
          ampText.style.color='red'
          movText.style.color='red'

      }else{
        wave=new Wave(0.07,Math.PI*2,16,2.5,5,1,1,1,1,1,1)
        waveType2D="sine wave"

        freqText.style.color='red'
        ampText.style.color='red'
        movText.style.color='red'
      }
 }

}
