
class Wave{


    maxTrueLong=false
    maxTrueAmp=false
  
    constructor(l, t, a, k,oscLmax, oscLmin ,oscLspeed,oscAmax, oscAmin ,oscAspeed,speedTimeIncrem, isZ = true){
      this.l=l
      this.t=t
      this.a=a
      this.k=k
      this.oscLmax=oscLmax
      this.oscLmin=oscLmin
      this.oscLspeed=oscLspeed
      this.oscAmax=oscAmax
      this.oscAmin=oscAmin
      this.oscAspeed=oscAspeed
      this.speedTimeIncrem=speedTimeIncrem
      this.isZ = isZ
    }
  
                              
    heigthFunction(x){ return (sin(this.horizontalFunction(x)*this.l+this.t) *this.a)}
  
  
    getParticlePos(x){
      return { x: this.horizontalFunction(x), y: this.heigthFunction(x)}
    }
    
    horizontalFunction(x){ return this.k * cos(x+this.t)+x}
  
  
    update(dT){ //delta Time

          this.t +=dT*this.speedTimeIncrem   
          this.t=this.t% (Math.PI*2)    
          
 
          if(this.l>=this.oscLmax || this.l <= this.oscLmin){
            this.maxTrueLong = ! this.maxTrueLong;
          }
          if(this.maxTrueLong){
            this.l-=this.oscLspeed*dT
      
          }else{
            this.l+=this.oscLspeed*dT
          }
        
          if(this.a>=this.oscAmax || this.a < this.oscAmin){
            this.maxTrueAmp = ! this.maxTrueAmp;
          }
          if(this.maxTrueAmp){
            this.a-=this.oscAspeed*dT
          }else{
            this.a+=this.oscAspeed*dT
          }
  
    }
  
  }
  

  class CircularWave extends Wave { 

    constructor(cx, cy, cz, l, t, a, k,oscLmax, oscLmin ,oscLspeed,oscAmax, oscAmin ,oscAspeed,speedTimeIncrem, isZ = true){
      this.l=l
      this.t=t
      this.a=a
      this.k=k
      this.oscLmax=oscLmax
      this.oscLmin=oscLmin
      this.oscLspeed=oscLspeed
      this.oscAmax=oscAmax
      this.oscAmin=oscAmin
      this.oscAspeed=oscAspeed
      this.speedTimeIncrem=-speedTimeIncrem
      this.isZ = isZ
      this.cx = cx;
      this.cy = cy;
      this.cz = cz;
    }
    

  }



  