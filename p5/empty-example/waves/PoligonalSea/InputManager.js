
var InputManager = {
    Keys:{
      WPressed : false,
      APressed : false,
      SPressed : false,
      DPressed : false,
    }
  
  }
  
  
  function keyPressed() {
  
    InputManager.Keys.APressed = keyCode == 65; 
    InputManager.Keys.WPressed = keyCode == 87; 
    InputManager.Keys.SPressed = keyCode == 83; 
    InputManager.Keys.DPressed = keyCode == 68; 
  
  
  }
  
  function keyReleased() {
  
    if(keyCode == 65) InputManager.Keys.APressed = false; 
    if(keyCode == 87) InputManager.Keys.WPressed = false; 
    if(keyCode == 83) InputManager.Keys.SPressed = false; 
    if(keyCode == 68) InputManager.Keys.DPressed = false; 
  
  
  }
  
  