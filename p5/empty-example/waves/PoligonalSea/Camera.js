
var cameraSettedInCorrectposition = false;


var Camera = {
    x:0,
    y:0,
    z:0,
    movSpeed:100,
    update : function(dt) {
      
      var mov = {x:0, y:0}
  
      if(InputManager.Keys.APressed){
        mov.x += -1;
      }
  
      if(InputManager.Keys.DPressed){
        mov.x += 1;
      }
  
      if(InputManager.Keys.WPressed){
        mov.y += -1;
      }
  
      if(InputManager.Keys.SPressed){
        mov.y += 1;
      }
  
      this.x += mov.x * dt * this.movSpeed;
      this.y += mov.y * dt * this.movSpeed;
    }
  }