

var d = 100 //distancia deseada 
var result=0

class Sea extends Drawable{

  constructor(waves, waveParticles, wavesSize, wavesCount){
    super()
    this.waves = waves;
    this.waveParticles = waveParticles;
    this.wavesize = wavesSize
    this.wavesCount = wavesCount
    this.ondaBase=true
    this.ondaCircular=false
    this.ondaLaGrange=false
    this.noOnda=false
  }

  draw(){

    
    
    beginShape(TRIANGLES) 
    for (let i = 0; i < this.waveParticles.length; i++) {
      if(!this.noOnda){
        this.waveParticles[i].x = this.waveParticles[i].initialPos.x;
        this.waveParticles[i].y = this.waveParticles[i].initialPos.y;
        this.waveParticles[i].z = this.waveParticles[i].initialPos.z;

        for (let w = 0; w < this.wavesCount; w++) {
          
          var newposZ = this.waves[w].getParticlePos(this.waveParticles[i].initialPos.z);
          var newposX = this.waves[w].getParticlePos(this.waveParticles[i].initialPos.x);
          
          
          let xwaveX = newposX.x;
          let xwaveY = newposX.y - this.waveParticles[i].initialPos.y;
          
          let zwaveZ = newposZ.x;
          let zwaveY = newposZ.y - this.waveParticles[i].initialPos.y;
          
          const scalarVerticlar = 0.2
          const scalarHorizontal = 0.2

          if(this.waves[w].isZ === true){
            this.waveParticles[i].z +=  zwaveZ*scalarHorizontal;
            this.waveParticles[i].y +=  zwaveY*scalarVerticlar;
          }else {
            
            this.waveParticles[i].x +=  xwaveX*scalarHorizontal;
            this.waveParticles[i].y +=  xwaveY*scalarVerticlar;
          }
        }

       if(this.ondaCircular){   
          // ACTUALIZAR ONDA CIRCULAR
          const circularWaveCenter = {
            x:200,
            y:-10,
            z:-400
          }
          var longitudOndaCircular =  0.01

          // anulamos la y en la distancia
          circularWaveCenter.y = this.waveParticles[i].initialPos.y; 
          //n es la distancia entre el punto central y dondestamos
          var n = distance(this.waveParticles[i].initialPos, circularWaveCenter); 
          result=Math.sin(n * longitudOndaCircular + this.waves[0].t) * 40;

          if(this.ondaLaGrange){  
            var lagrangeFactor = 0;

            // var d = 100 //distancia deseada 
            var offset = 100 

            if(n > d-offset && n < d+offset){
              lagrangeFactor = LaGrangeWave(n,d,offset);
            }
            var di=Math.PI/(2*d)
            result =lagrangeFactor * Math.sin(n* longitudOndaCircular + di) * 40;
            
            if(d>winWidth+winHeig){
              d=100
            }else{
              d+=0.02
            }
        }
        
        // APLICAR ONDA CIIRCULAR JUNTO CON LAS OTRAS
        if(this.ondaBase && this.ondaCircular){
         this.waveParticles[i].y += result;
        }   
        if(this.ondaCircular && !this.ondaBase){
        // ANULAR LAS OTRAS Y APLICAR SOLO LA CIRCULAR (recuerda q el speed del tiempo tiene q ser negativo)
        this.waveParticles[i].y = result;
        this.waveParticles[i].x = this.waveParticles[i].initialPos.x;
        this.waveParticles[i].z = this.waveParticles[i].initialPos.z;
        }
      }   

    }

    if(!cameraSettedInCorrectposition){
      Camera.y = this.waveParticles[i].y;
      cameraSettedInCorrectposition = true;
    }
            

          if(i%this.wavesize == this.wavesize-1)
            continue;
          
          var jplusI = i + this.wavesize
          if(jplusI > (this.wavesize * this.wavesize) -(this.wavesize+1))
            continue;
      
            vertex(this.waveParticles[jplusI].x, this.waveParticles[jplusI].y, this.waveParticles[jplusI].z);
            vertex(this.waveParticles[i].x,this.waveParticles[i].y, this.waveParticles[i].z);
            vertex(this.waveParticles[i+1].x,this.waveParticles[i+1].y, this.waveParticles[i+1].z);

            
            vertex(this.waveParticles[i+1].x,this.waveParticles[i+1].y, this.waveParticles[i+1].z);
            vertex(this.waveParticles[jplusI].x,this.waveParticles[jplusI].y, this.waveParticles[jplusI].z);
            vertex(this.waveParticles[jplusI+1].x,this.waveParticles[jplusI+1].y, this.waveParticles[jplusI+1].z);
             
    }
    endShape();

  }

}



var maxL = 0.05;
var minL = 0.04;

var minLSpeed = 0.002;
var maxLSpeed = 0.001;

var maxA = 20;
var minA = -maxA;

var minASpeed = 14;
var maxASpeed = 16;

var sea;

function  seaSetup() {

  var waveParticles = [];
  var waveSize = 30
  var wavesCount = 10
  var waves = []
  

    var useZ = false; 

   for (let i = 0; i < wavesCount; i++) {
     
     
     waves.push( new Wave(
      random(maxL),        //l 
      Math.PI*2,           //t 
      random(winWidth/60), //a 
      -1,         //k
      random(maxL),        //oscLmax, 
      random(minL),        //oscLmin 
      randomRange(minLSpeed, maxLSpeed), //oscLspeed,
      random(maxA),        //oscAmax, 
      random(minA),        //oscAmin
      randomRange(minASpeed, maxASpeed),  //oscAspeed
      random(0.5),        //speedTimeIncrem
      useZ
      ));
      useZ = !useZ

      waves[i].k = waves[i].oscAmax * 10
    }

    for(let i=0; i<waveSize;i++){
      for(let k=0; k<waveSize;k++){
    
      var c1 = new WaveParticle((-winWidth/2)+i*50, 120, -k*50,2);
    
      c1.initialPos = {x:c1.x, y:c1.y, z:c1.z}
      
      waveParticles.push(c1)
    
      }
    }
  
    
    sea = new Sea(waves, waveParticles, waveSize, wavesCount)

}