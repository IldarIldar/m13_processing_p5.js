var drawableObjects = [];
class Drawable {
  constructor(){
    drawableObjects.push(this)
  }
}

function drawDrawableObjects() {
    translate(-Camera.x, -Camera.y, -Camera.z)
    drawableObjects.forEach((obj) =>{
      obj.draw();
    })
    translate(Camera.x, Camera.y, Camera.z)
    
}

