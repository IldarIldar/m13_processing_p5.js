let v = [];

let rows = 60, cols = 120;

let canvas;

let pNumSlider, pLenSlider, diameterSlider, pSharpSlider, horPertSlider;
let petalNum, pLength, diameter, pSharpness, hPert, horPert;

let heightSlider, curvatureSlider1, curvatureSlider2;
let flowerHeight, curvature1, curvature2;

let bumpSlider, bumpNumSlider;
let bump, bumpNum;

let pNum, fD, pLen, pSharp;
let fHeight, curve1, curve2;
let b, bNum;

let colorDef=360

function setup(){
  frameRate(5)
  canvas = createCanvas(700, 700, WEBGL);
 // canvas.class("canvas");
  canvas.parent('script-container')


  //colorMode(HSB, 360, 100, 100);

  angleMode(DEGREES);
  noStroke();

  petalNum = createDiv();
  petalNum.class("valueDisplay");
  petalNum.parent('parameters')
  pNumSlider = createSlider(0, 20, 5, 1);
  pNumSlider.class("Slider");
  pNumSlider.parent('parameters')

  diameter = createDiv();
  diameter.class("valueDisplay");
  diameter.parent('parameters')
  diameterSlider = createSlider(0, 250, 200, 10);
  diameterSlider.class("Slider");
  diameterSlider.parent('parameters')


  pLength = createDiv();
  pLength.class("valueDisplay");
  pLength.parent('parameters')
  pLenSlider = createSlider(0, 300, 60, 10);
  pLenSlider.class("Slider");
  pLenSlider.parent('parameters')


  pSharpness = createDiv();
  pSharpness.class("valueDisplay");
  pSharpness.parent('parameters')
  pSharpSlider = createSlider(0.0, 10.0, 0.4, 0.1);
  pSharpSlider.class("Slider");
  pSharpSlider.parent('parameters')


  flowerHeight = createDiv();
  flowerHeight.class("valueDisplay");
  flowerHeight.parent('parameters')
  heightSlider = createSlider(0, 600, 300, 10);
  heightSlider.class("Slider");
  heightSlider.parent('parameters')


  curvature1 = createDiv();
  curvature1.class("valueDisplay");
  curvature1.parent('parameters')
  curvatureSlider1 = createSlider(0.0, 4.0, 0.8, 0.1);
  curvatureSlider1.class("Slider");
  curvatureSlider1.parent('parameters')


  curvature2 = createDiv();
  curvature2.class("valueDisplay");
  curvature2.parent('parameters')
  curvatureSlider2 = createSlider(0.0, 1.0, 0.2, 0.05);
  curvatureSlider2.class("Slider");
  curvatureSlider2.parent('parameters')


  bump = createDiv();
  bump.class("valueDisplay");
  bump.parent('parameters')
  bumpSlider = createSlider(0.0, 5.0, 2.5, 0.5);
  bumpSlider.class("Slider");
  bumpSlider.parent('parameters')


  bumpNum = createDiv();
  bumpNum.class("valueDisplay");
  bumpNum.parent('parameters')
  bumpNumSlider = createSlider(0, 20, 10, 1);
  bumpNumSlider.class("Slider");
  bumpNumSlider.parent('parameters')

  horPert = createDiv();
  horPert.class("valueDisplay");
  horPert.parent('parameters')
  horPertSlider = createSlider(0, 120, 0, 1);
  horPertSlider.class("Slider");
  horPertSlider.parent('parameters')



  color = createDiv();
  color.class("valueDisplay");
  color.parent('parameters')
  colorSlider = createSlider(0, 1100, 360, 10);
  colorSlider.class("Slider");
  colorSlider.parent('parameters')

  explanation = createDiv();
  explanation.class("valueDisplay");
  explanation.parent('parameters')


  rowsD = createDiv();
  rowsD.class("valueDisplay");
  rowsD.parent('parameters')
  rowsSlider = createSlider(0, 60, 60, 1);
  rowsSlider.class("Slider");
  rowsSlider.parent('parameters')

  colsD = createDiv();
  colsD.class("valueDisplay");
  colsD.parent('parameters')
  colsSlider = createSlider(0, 120, 120, 1);
  colsSlider.class("Slider");
  colsSlider.parent('parameters')
  



}

function draw(){

  clear();
  orbitControl(4, 4);

  rotateX(60);

  pNum = pNumSlider.value();
  fD = diameterSlider.value();
  pLen = pLenSlider.value();
  pSharp = pSharpSlider.value();

  fHeight = heightSlider.value();
  curve1 = curvatureSlider1.value();
  curve2 = curvatureSlider2.value();

  b = bumpSlider.value();
  bNum = bumpNumSlider.value();

  colorDef=colorSlider.value()

  rows=rowsSlider.value()
  cols=colsSlider.value()

  hPert=horPertSlider.value()

  //verticalOndulation=1000



  colorMode(HSB, colorDef, 100, 100);

  for(theta = 0; theta < rows; theta += 1){
    v.push([]);
    for(let phi = 0; phi < cols; phi += 1){
      let r =(pLen*pow(abs(sin(pNum/2*phi*360/cols)),pSharp)+fD +abs(sin(phi*hPert)*7)
      )* theta/rows ;
      let x = r * cos(phi*360/cols);
      let y = r * sin(phi*360/cols);
      let z = vShape(fHeight, r/100, curve1, curve2, 1.5) - 200+
        bumpiness(b, r/100, bNum, phi*360/cols)//*cos(theta*verticalOndulation)*(30/(theta/1));
  
        let pos = createVector(x, y, z);
        v[theta].push(pos);
      
  

    }
  }


  for(let theta = 0; theta < v.length; theta++){
    for(let phi = 0; phi < v[theta].length; phi++){
      fill(340, 100-theta, 100);
      if(theta < v.length-1 && phi < v[theta].length-1){
        beginShape();
        vertex(v[theta][phi].x, v[theta][phi].y, v[theta][phi].z);
        vertex(v[theta+1][phi].x, v[theta+1][phi].y, v[theta+1][phi].z);
        vertex(v[theta+1][phi+1].x, v[theta+1][phi+1].y, v[theta+1][phi+1].z);
        vertex(v[theta][phi+1].x, v[theta][phi+1].y, v[theta][phi+1].z);
        endShape(CLOSE);
      }else if(theta < v.length-1 && phi == v[theta].length-1){
        beginShape();
        vertex(v[theta][phi].x, v[theta][phi].y, v[theta][phi].z);
        vertex(v[theta][0].x, v[theta][0].y, v[theta][0].z);
        vertex(v[theta+1][0].x, v[theta+1][0].y, v[theta+1][0].z);
        vertex(v[theta+1][phi].x, v[theta+1][phi].y, v[theta+1][phi].z);
        endShape(CLOSE);
      }
    }
  }



  
  


  petalNum.html("Number of the petals: " + pNumSlider.value());
  diameter.html("Diameter: " + diameterSlider.value());
  pLength.html("Petal length: " + pLenSlider.value());
  pSharpness.html("Petal sharpness: " + pSharpSlider.value());

  flowerHeight.html("Flower height: " + heightSlider.value());
  curvature1.html("Curvature 1: " + curvatureSlider1.value());
  curvature2.html("Curvature 2: " + curvatureSlider2.value());

  bump.html("Bumpiness: " + bumpSlider.value());
  bumpNum.html("Bumpiness number: " + bumpNumSlider.value());

  horPert.html("border perturbation "+horPertSlider.value())

  color.html("color")
    
  explanation.html("In case of slow performance try change definition params")

  rowsD.html("horrizontal definition "+rowsSlider.value())

  colsD.html("vertical definition "+colsSlider.value())



  v = [];
}

function vShape(A, r, a, b, c){
  return A*pow(Math.E, -b*pow(abs(r), c))*pow(abs(r), a);
}

function bumpiness(A, r, f, angle){
  return 1 + A * pow(r, 2) * sin(f * angle);
}


